package test;

import gui.SoftwareFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.CashCard;

public class SoftwareTest {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		CashCard cash = new CashCard(0);
		cash.refill(200);
		frame.setResult("Refill : 200");
		frame.extendResult("Balance : " + cash.getBalance());
		cash.buy(60);
		frame.extendResult("Buy Food : 60");
		frame.extendResult("Balance : " + cash.getBalance());
	}

	ActionListener list;
	SoftwareFrame frame;
}
