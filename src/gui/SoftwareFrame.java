package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class SoftwareFrame extends JFrame{
	private JLabel showProjectName;
	private JTextArea showResults;
	private JButton endButton;
	private String str;


	public SoftwareFrame() {
		createFrame();
	}

	public void createFrame() {
		this.showProjectName = new JLabel("Lab01");
		this.showResults = new JTextArea("your resutls will be showed here");
		this.endButton = new JButton("end program");
		setLayout(new BorderLayout());
		add(this.showProjectName, BorderLayout.NORTH);
		add(this.showResults, BorderLayout.CENTER);
		add(this.endButton, BorderLayout.SOUTH);
	}

	public void setProjectName(String s) {
		showProjectName.setText(s);
	}

	public void setResult(String str) {
		this.str = str;
		showResults.setText(this.str);
	}
	

	public void extendResult(String str) {
		this.str = this.str + "\n" + str;
		showResults.setText(this.str);
	}

	public void setListener(ActionListener list) {
		endButton.addActionListener(list);
	}
}
