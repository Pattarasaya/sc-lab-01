package model;

public class CashCard {
	
	private int balance;
	
	public CashCard(int balance){
		this.balance = balance;
	}

	public void refill(int amount) {
		balance = balance + amount;
	}

	public void buy(int amount) {
		balance = balance - amount;
	}

	public int getBalance() {
		return this.balance;
	}
}
